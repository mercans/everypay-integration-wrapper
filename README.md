# README #

### What is this repository for? ###

* Making https://github.com/UnifiedPaymentSolutions/everypay-integration useful.

### How do I get set up? ###

* Install via composer:


    "require": {
        "mercans/everypay-integration-wrapper": "dev-master",
        "UnifiedPaymentSolutions/everypay-integration": "dev-master#04ba3782778025d5c2bacc45649e161a73f1a27e"
    },
    "repositories": [
        {
            "type": "vcs",
            "url": "https://bitbucket.org/mercans/everypay-integration-wrapper.git"
        },
        {
            "type": "vcs",
            "url": "https://github.com/UnifiedPaymentSolutions/everypay-integration"
        }
    ]
