<?php

namespace Mercans\Everypay;

abstract class ApiAdapter
{
    /**
     * @var \EveryPay
     */
    private $adapter;

    /**
     * @var string
     */
    private $api_username;

    /**
     * @var string
     */
    private $api_secret;

    /**
     * @param string $api_username
     * @param string $api_secret
     */
    public function __construct($api_username, $api_secret)
    {
        $this->adapter = new \EveryPay();

        $this->api_username = $api_username;

        $this->api_secret = $api_secret;
    }

    /**
     * @return \EveryPay
     */
    protected function getAdapter()
    {
        return $this->adapter;
    }

    /**
     * @return string
     */
    protected function getApiUsername()
    {
        return $this->api_username;
    }

    /**
     * @return string
     */
    protected function getApiSecret()
    {
        return $this->api_secret;
    }
}
