<?php

namespace Mercans\Everypay;

abstract class ApiFields extends ApiAdapter
{
    /**
     * @var string[]
     */
    protected $params;

    /**
     * @param string $name
     * @param string $value
     */
    public function setParam($name, $value)
    {
        $this->params[$name] = $value;
    }

    /**
     * @return \string[]
     */
    public function getParams()
    {
        return $this->params;
    }

    /**
     * @param \string[] $params
     */
    public function setParams($params)
    {
        $this->params = $params;
    }

    /**
     * @return string[]
     */
    public function getSignedFields()
    {
        return $this->getAdapter()->getFields($this->getParams());
    }
}
