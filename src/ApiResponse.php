<?php

namespace Mercans\Everypay;

class ApiResponse extends ApiAdapter
{
    /**
     * @see \EveryPay::_VERIFY_SUCCESS
     */
    const STATE_SETTLED = 'settled';

    /**
     * @see \EveryPay::_VERIFY_SUCCESS
     */
    const STATE_AUTHORISED = 'authorised';

    /**
     * @see \EveryPay::_VERIFY_CANCEL
     */
    const STATE_CANCELLED = 'cancelled';

    /**
     * @see \EveryPay::_VERIFY_CANCEL
     */
    const STATE_WAITING_FOR_3DS_RESPONSE = 'waiting_for_3ds_response';

    /**
     * @see \EveryPay::_VERIFY_FAIL
     */
    const STATE_FAILED = 'failed';

    /**
     * @var string[]
     */
    private $data;

    /**
     * @var int
     */
    private $verify_status;

    /**
     * @var string[]
     */
    private $messages;

    /**
     * @param string $api_username
     * @param string $api_secret
     * @param string[] $data
     */
    public function __construct($api_username, $api_secret, $data)
    {
        parent::__construct($api_username, $api_secret);

        $this->getAdapter()->init($this->getApiUsername(), $this->getApiSecret());

        $this->data = $data;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getEncodedData();
    }

    public function getId()
    {
        return $this->data['payment_reference'];
    }

    public function getOrderReference()
    {
        return $this->data['order_reference'];
    }

    /**
     * @return string JSON
     */
    private function getEncodedData()
    {
        return json_encode($this->data, JSON_FORCE_OBJECT);
    }

    /**
     * @return bool
     */
    public function isValid()
    {
        try {
            $this->verify_status = $this->getAdapter()->verify($this->data);
        } catch (\Exception $exception) {
            $this->messages[] = $exception->getMessage();

            return false;
        }

        return true;
    }

    /**
     * @return int
     */
    public function getVerifyStatus()
    {
        return $this->verify_status;
    }

    /**
     * @return \string[]
     */
    public function getMessages()
    {
        return $this->messages;
    }

    /**
     * @return mixed
     */
    public function getValue($field)
    {
        return $this->data[$field];
    }
}
