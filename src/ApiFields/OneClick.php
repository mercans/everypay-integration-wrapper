<?php

namespace Mercans\Everypay\ApiFields;

use Mercans\Everypay\ApiFields;

class OneClick extends ApiFields
{
    /**
     * @param string $api_username
     * @param string $api_secret
     */
    public function __construct($api_username, $api_secret)
    {
        parent::__construct($api_username, $api_secret);

        $this->getAdapter()->init($this->getApiUsername(), $this->getApiSecret(), [
            'transaction_type' => 'charge'
        ]);
    }
}
